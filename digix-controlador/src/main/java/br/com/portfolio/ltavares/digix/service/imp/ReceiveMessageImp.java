package br.com.portfolio.ltavares.digix.service.imp;

import br.com.portfolio.ltavares.digix.service.interfaces.IReceiveMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class ReceiveMessageImp implements IReceiveMessage {

   @Autowired
   private final FamiliaServiceImp familiaServiceImp;

    public ReceiveMessageImp(FamiliaServiceImp familiaServiceImp) {
        this.familiaServiceImp = familiaServiceImp;
    }

    @JmsListener(destination = "controladorFamilia")
    @Override
    public void receiveMessage(UUID uuid)  {
        try {
            log.info("PROCESSANDO UUID: " + uuid);
            familiaServiceImp.validaFamilia(uuid);
        } catch (Exception e) {
            throw new RuntimeException("Erro ao processar item da fila: " + uuid);
        }
    }


}
