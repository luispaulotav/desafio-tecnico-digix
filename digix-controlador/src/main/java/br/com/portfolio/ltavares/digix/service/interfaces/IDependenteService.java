package br.com.portfolio.ltavares.digix.service.interfaces;

import br.com.portfolio.ltavares.digix.dto.FamiliaContempladoDTO;

public interface IDependenteService {

    FamiliaContempladoDTO validaPessoasDependentesMenoresIdade(FamiliaContempladoDTO familiaDTO);
}
