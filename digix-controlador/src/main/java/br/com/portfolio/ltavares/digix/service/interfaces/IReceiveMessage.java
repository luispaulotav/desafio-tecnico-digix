package br.com.portfolio.ltavares.digix.service.interfaces;

import java.util.UUID;

public interface IReceiveMessage {

    void receiveMessage(UUID uuid);
}
