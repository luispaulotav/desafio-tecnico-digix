package br.com.portfolio.ltavares.digix.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContempladoDTO {

    private UUID identificadorFamilia;

    private int qunatidadeCriterio;

    private int pontuacaoTotal;

    private LocalDate dataFamilaSelecionada;
}
