package br.com.portfolio.ltavares.digix.service.imp;

import br.com.portfolio.ltavares.digix.dto.FamiliaContempladoDTO;
import br.com.portfolio.ltavares.digix.dto.PessoaContempladoDTO;
import br.com.portfolio.ltavares.digix.enumaration.IdadePretendente;
import br.com.portfolio.ltavares.digix.enumaration.TipoVinculo;
import br.com.portfolio.ltavares.digix.enumaration.ValorRenda;
import br.com.portfolio.ltavares.digix.service.interfaces.IConjugueService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.EnumSet;

@Service
public class ConjugueServiceImp implements IConjugueService {

    @Override
    public FamiliaContempladoDTO calculaPontuacaoPorFamilia(FamiliaContempladoDTO familiaDTO) {
        familiaDTO.getPessoas().forEach(pessoaDTO ->{
            if(EnumSet.of(TipoVinculo.CONJUGUE, TipoVinculo.PRETENDENTE).contains(pessoaDTO.getTipoVinculo())) {
                getPontuacaoPorIdade(pessoaDTO);
                getPontuacaoPorRenda(pessoaDTO);
                familiaDTO.setPontuacao(familiaDTO.getPontuacao() + pessoaDTO.getPontuacao());
                familiaDTO.setQuantidadeCriterio(familiaDTO.getQuantidadeCriterio() + pessoaDTO.getQuantidadeCriterio());
            }
        });
        return familiaDTO;
    }

    private int calularIdade(LocalDate dataNascimento) {
        return LocalDate.now().getYear() - dataNascimento.getYear();
    }

    private PessoaContempladoDTO getPontuacaoPorRenda(PessoaContempladoDTO pessoaDTO) {
        int pontuacao =  ValorRenda.getPontuacaoPorValorRenda(pessoaDTO.getRenda().getValor());

        if (pontuacao > 0 ) {
            pessoaDTO.setPontuacao(pessoaDTO.getPontuacao() + pontuacao);
            pessoaDTO.setQuantidadeCriterio(pessoaDTO.getQuantidadeCriterio() + 1);
        }
        return pessoaDTO;
    }

    private PessoaContempladoDTO getPontuacaoPorIdade(PessoaContempladoDTO pessoaDTO) {
        int pontuacao = IdadePretendente.getPontuacaoPorIdade(calularIdade(pessoaDTO.getDataNascimento()));
        if(pontuacao > 0) {
            pessoaDTO.setPontuacao(pessoaDTO.getPontuacao() + pontuacao);
            pessoaDTO.setQuantidadeCriterio(pessoaDTO.getQuantidadeCriterio() + 1);
        }
        return pessoaDTO;
    }
}
