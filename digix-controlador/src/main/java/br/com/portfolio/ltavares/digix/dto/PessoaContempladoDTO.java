package br.com.portfolio.ltavares.digix.dto;

import br.com.portfolio.ltavares.digix.enumaration.TipoVinculo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PessoaContempladoDTO {

    private String nome;

    private LocalDate dataNascimento;

    private TipoVinculo tipoVinculo;

    private RendaContempladoDTO renda;

    private UUID identificacaoFamilia;

    private int quantidadeCriterio;

    private int pontuacao;
}
