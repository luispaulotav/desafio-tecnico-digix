package br.com.portfolio.ltavares.digix.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/controladores")
public class ControladorResource {

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping("/receive/{uuid}")
    public ResponseEntity<?> receive(@PathVariable("uuid") UUID uuid) {
        this.jmsTemplate.convertAndSend("controladorFamilia", uuid);
        return ResponseEntity.ok().build();
    }
}
