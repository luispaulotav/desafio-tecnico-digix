package br.com.portfolio.ltavares.digix.service.imp;

import br.com.portfolio.ltavares.digix.dto.FamiliaContempladoDTO;
import br.com.portfolio.ltavares.digix.dto.PessoaContempladoDTO;
import br.com.portfolio.ltavares.digix.enumaration.TipoVinculo;
import br.com.portfolio.ltavares.digix.service.interfaces.IDependenteService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class DependenteServiceImp implements IDependenteService {

    @Override
    public FamiliaContempladoDTO validaPessoasDependentesMenoresIdade(FamiliaContempladoDTO familiaDTO){
        List<PessoaContempladoDTO> dependentesMenorIdade = new ArrayList<>();

        familiaDTO.getPessoas().forEach(pessoaContempladoDTO -> {
            if(TipoVinculo.DEPENDENTE.equals(pessoaContempladoDTO.getTipoVinculo())) {
                if (ehMenorIdade(pessoaContempladoDTO.getDataNascimento())) {
                    dependentesMenorIdade.add(pessoaContempladoDTO);
                }
            }
        });
        int pontuacao = calculaPontuacao(dependentesMenorIdade);
        familiaDTO.setPontuacao(familiaDTO.getPontuacao() + pontuacao);
        if(pontuacao > 0){
            familiaDTO.setQuantidadeCriterio(familiaDTO.getQuantidadeCriterio()+1);
        }
        return familiaDTO;
    }

    private int calularIdade(LocalDate dataDeNascimento) {
        return LocalDate.now().getYear() - dataDeNascimento.getYear();
    }

    private boolean ehMenorIdade(LocalDate dataNascimento) {
        return calularIdade(dataNascimento) < 18;
    }

    private int calculaPontuacao(List<PessoaContempladoDTO> pessoas) {
        if (pessoas.size() == 1 || pessoas.size() == 2) {
            return 2;
        }
        if (pessoas.size() >= 3) {
            return 3;
        }
        return 0;
    }

}
