package br.com.portfolio.ltavares.digix.service.interfaces;

import java.util.UUID;

public interface IFamiliaService {

    void validaFamilia(UUID uuid);
}
