package br.com.portfolio.ltavares.digix.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FamiliaContempladoDTO {

    private UUID identificaoFamilia;

    private List<PessoaContempladoDTO> pessoas;

    private int quantidadeCriterio;

    private int pontuacao;

}

