package br.com.portfolio.ltavares.digix.service.imp;

import br.com.portfolio.ltavares.digix.dto.FamiliaContempladoDTO;
import br.com.portfolio.ltavares.digix.dto.PessoaContempladoDTO;
import br.com.portfolio.ltavares.digix.dto.RendaContempladoDTO;
import br.com.portfolio.ltavares.digix.enumaration.Status;
import br.com.portfolio.ltavares.digix.model.Contemplado;
import br.com.portfolio.ltavares.digix.model.Familia;
import br.com.portfolio.ltavares.digix.model.Pessoa;
import br.com.portfolio.ltavares.digix.repository.ContempladoRepository;
import br.com.portfolio.ltavares.digix.repository.FamiliaRepository;
import br.com.portfolio.ltavares.digix.service.interfaces.IFamiliaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class FamiliaServiceImp implements IFamiliaService {

    private final FamiliaRepository familiaRepository;

    private final ConjugueServiceImp conjugueServiceImp;

    private final DependenteServiceImp dependenteServiceImp;

    private final ContempladoRepository contempladoRepository;

    @Autowired
    public FamiliaServiceImp(FamiliaRepository familiaRepository, ConjugueServiceImp conjugueServiceImp, DependenteServiceImp dependenteServiceImp, ContempladoRepository contempladoRepository) {
        this.familiaRepository = familiaRepository;
        this.conjugueServiceImp = conjugueServiceImp;
        this.dependenteServiceImp = dependenteServiceImp;
        this.contempladoRepository = contempladoRepository;
    }

    @Override
    public void validaFamilia(UUID uuid) {
        Optional<Familia> familiaOpt = findByIdAndStatus(uuid, Status.CADASTROVALIDO);
        validaTipoVinculoFamilia(familiaOpt.get());
        alterarStatusCadastroFamilia(familiaOpt.get(), Status.SELECIONADOEMOUTROPROCESSO);
    }

    private Optional<Familia> findByIdAndStatus(UUID uuid, Status status) {
        return Optional.ofNullable(this.familiaRepository.findByIdAndStatus(uuid, status))
                .orElseThrow(() -> new RuntimeException("Não foi encontrado familia com id "+ uuid
                        + " ou o cadastro não está com Status igual a Cadastro Válido" ));
    }

    private void validaTipoVinculoFamilia(Familia familia) {
        FamiliaContempladoDTO familiaDTO = buildFamiliaContempladoDTO(familia);
        validaPessoaConjugue(familiaDTO);
        validaPessoaDependente(familiaDTO);
        buildContemplado(familiaDTO);
    }

    private void alterarStatusCadastroFamilia(Familia familia, Status status) {
        familia.setStatus(status);
        familiaRepository.save(familia);
    }

    private FamiliaContempladoDTO buildFamiliaContempladoDTO(Familia familia) {
        return FamiliaContempladoDTO.builder()
                .pessoas(buildPessoasParaPessoasContemplados(familia.getPessoas()))
                .identificaoFamilia(familia.getId())
                .build();
    }

    private List<PessoaContempladoDTO> buildPessoasParaPessoasContemplados(List<Pessoa> pessoas) {
        List<PessoaContempladoDTO> pessoasDto = new ArrayList<>();
        pessoas.forEach(pessoa -> {
            pessoasDto.add(buildPessoaDTO(pessoa));
       });
        return pessoasDto;
    }

    private void buildContemplado(FamiliaContempladoDTO familiaDto) {
         Contemplado contemplado = Contemplado.builder()
                .dataFamilaSelecionada(LocalDate.now())
                .identificadorFamilia(familiaDto.getIdentificaoFamilia())
                .pontuacaoTotal(familiaDto.getPontuacao())
                .quantidadeCriterio(familiaDto.getQuantidadeCriterio())
                .build();
         contempladoRepository.save(contemplado);
    }

    private PessoaContempladoDTO buildPessoaDTO(Pessoa pessoa) {
        return PessoaContempladoDTO.builder()
                .dataNascimento(pessoa.getDataNascimento())
                .nome(pessoa.getNome())
                .tipoVinculo(pessoa.getTipoVinculo())
                .renda(buildRendaDTO(pessoa))
                .build();
    }

    private RendaContempladoDTO buildRendaDTO(Pessoa pessoa) {
        return RendaContempladoDTO.builder()
                .valor(pessoa.getRenda().getValor())
                .build();
    }

    private FamiliaContempladoDTO validaPessoaDependente(FamiliaContempladoDTO familiaDTO) {
        return dependenteServiceImp.validaPessoasDependentesMenoresIdade(familiaDTO);
    }

    private FamiliaContempladoDTO validaPessoaConjugue(FamiliaContempladoDTO familiaDTO) {
        return conjugueServiceImp.calculaPontuacaoPorFamilia(familiaDTO);
    }
}
