package br.com.portfolio.ltavares.digix.service;

import br.com.portfolio.ltavares.digix.dto.FamiliaContempladoDTO;
import br.com.portfolio.ltavares.digix.enumaration.Status;
import br.com.portfolio.ltavares.digix.model.Contemplado;
import br.com.portfolio.ltavares.digix.model.Familia;
import br.com.portfolio.ltavares.digix.repository.ContempladoRepository;
import br.com.portfolio.ltavares.digix.repository.FamiliaRepository;
import br.com.portfolio.ltavares.digix.service.imp.ConjugueServiceImp;
import br.com.portfolio.ltavares.digix.service.imp.DependenteServiceImp;
import br.com.portfolio.ltavares.digix.service.imp.FamiliaServiceImp;
import br.com.portfolio.ltavares.digix.utils.MockUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FamiliaServiceImpTest {

    @InjectMocks
    private FamiliaServiceImp familiaServiceImp;

    @Mock
    private FamiliaRepository familiaRepository;

    @Mock
    private DependenteServiceImp dependenteServiceImp;

    @Mock
    private ConjugueServiceImp conjugueServiceImp;

    @Mock
    private ContempladoRepository contempladoRepository;

    private FamiliaContempladoDTO familiaContempladoDTO;

    private Familia familia;

    private Contemplado contemplado;

    @Before
    public void init(){
       familiaContempladoDTO = MockUtils.buildFamiliacontemplado();
       familia = MockUtils.buildFamilia();
       familia.setId(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));
       contemplado = MockUtils.buildContemplado(familiaContempladoDTO);
    }

    @Test
    public void validaFamiliaTest() {
        when(familiaRepository.findByIdAndStatus(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"), Status.CADASTROVALIDO))
                .thenReturn(Optional.of(familia));
        when(dependenteServiceImp.validaPessoasDependentesMenoresIdade(familiaContempladoDTO)).thenReturn(familiaContempladoDTO);
        when(conjugueServiceImp.calculaPontuacaoPorFamilia(familiaContempladoDTO)).thenReturn(familiaContempladoDTO);
        when(contempladoRepository.save(contemplado)).thenReturn(contemplado);
        familiaServiceImp.validaFamilia(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));

        assertEquals(familiaContempladoDTO.getPessoas().get(0).getNome(), familia.getPessoas().get(0).getNome());
        assertEquals(familiaContempladoDTO.getPessoas().get(0).getDataNascimento(), familia.getPessoas().get(0).getDataNascimento());
        assertEquals(familiaContempladoDTO.getPessoas().get(0).getTipoVinculo(), familia.getPessoas().get(0).getTipoVinculo());
    }

}
