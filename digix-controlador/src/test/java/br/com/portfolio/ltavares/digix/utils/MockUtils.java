package br.com.portfolio.ltavares.digix.utils;

import br.com.portfolio.ltavares.digix.dto.FamiliaContempladoDTO;
import br.com.portfolio.ltavares.digix.dto.PessoaContempladoDTO;
import br.com.portfolio.ltavares.digix.dto.RendaContempladoDTO;
import br.com.portfolio.ltavares.digix.enumaration.Status;
import br.com.portfolio.ltavares.digix.enumaration.TipoVinculo;
import br.com.portfolio.ltavares.digix.model.Contemplado;
import br.com.portfolio.ltavares.digix.model.Familia;
import br.com.portfolio.ltavares.digix.model.Pessoa;
import br.com.portfolio.ltavares.digix.model.Renda;

import java.time.LocalDate;
import java.util.Arrays;

public class MockUtils {

    public static Contemplado buildContemplado(FamiliaContempladoDTO familiaContempladoDTO){
        return Contemplado.builder()
                .identificadorFamilia(familiaContempladoDTO.getIdentificaoFamilia())
                .quantidadeCriterio(familiaContempladoDTO.getQuantidadeCriterio())
                .dataFamilaSelecionada(LocalDate.now())
                .pontuacaoTotal(familiaContempladoDTO.getPontuacao())
                .build();
    }

    public static FamiliaContempladoDTO buildFamiliacontemplado(){
        return FamiliaContempladoDTO.builder()
                .pessoas(Arrays.asList(buildPessoaContemplado()))
                .build();
    }

    private static PessoaContempladoDTO buildPessoaContemplado() {
        return PessoaContempladoDTO.builder()
                .dataNascimento(LocalDate.of(1990,11,24))
                .tipoVinculo(TipoVinculo.PRETENDENTE)
                .nome("Jose Antonio da Silva")
                .renda(buildRendaContemplado())
                .build();
    }

    private static RendaContempladoDTO buildRendaContemplado() {
        return RendaContempladoDTO.builder()
                .valor(1900.0)
                .build();
    }

    public static Familia buildFamilia(){
        return Familia.builder()
                .pessoas(Arrays.asList(buildPessoa()))
                .status(Status.CADASTROIMCOMPLETO)
                .build();
    }

    private static Pessoa buildPessoa() {
        return Pessoa.builder()
                .dataNascimento(LocalDate.of(1990,11,24))
                .tipoVinculo(TipoVinculo.PRETENDENTE)
                .nome("Jose Antonio da Silva")
                .renda(buildRenda())
                .build();
    }

    private static Renda buildRenda() {
        return Renda.builder()
                .valor(1900.0)
                .build();
    }
}
