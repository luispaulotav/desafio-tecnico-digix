package br.com.portfolio.ltavares.digix.service.imp;

import br.com.portfolio.ltavares.digix.model.Contemplado;
import br.com.portfolio.ltavares.digix.repository.ContempladoRepository;
import br.com.portfolio.ltavares.digix.service.interfaces.IContempladoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.Cacheable;


@Service
public class ContempladoServiceImp implements IContempladoService {

    private final ContempladoRepository contempladoRepository;

    @Autowired
    public ContempladoServiceImp(ContempladoRepository contempladoRepository) {
        this.contempladoRepository = contempladoRepository;
    }

    @Cacheable("contemplados")
    public Page<Contemplado> buscarContemplados(Pageable pageable) {
        return this.contempladoRepository.findAll(pageable);
    }
}
