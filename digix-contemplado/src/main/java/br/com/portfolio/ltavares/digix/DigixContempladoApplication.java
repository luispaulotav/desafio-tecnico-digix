package br.com.portfolio.ltavares.digix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class DigixContempladoApplication {

    public static void main(String[] args) { SpringApplication.run(DigixContempladoApplication.class, args); }
}
