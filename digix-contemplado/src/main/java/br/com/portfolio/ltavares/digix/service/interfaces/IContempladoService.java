package br.com.portfolio.ltavares.digix.service.interfaces;

import br.com.portfolio.ltavares.digix.model.Contemplado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IContempladoService {

    Page<Contemplado> buscarContemplados(Pageable pageable);
}
