package br.com.portfolio.ltavares.digix.resource;

import br.com.portfolio.ltavares.digix.model.Contemplado;
import br.com.portfolio.ltavares.digix.service.imp.ContempladoServiceImp;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/contemplados")
public class ContempladoResource {

    @Autowired
    private ContempladoServiceImp contempladoServiceImp;

    @GetMapping(value = "/buscartodos", produces = "application/json")
    @ApiOperation(value = "Buscar todas familias contempladas", response = Contemplado.class)
    public ResponseEntity<?> buscaFamilias(Pageable pageable) {
        return ResponseEntity.ok(this.contempladoServiceImp.buscarContemplados(pageable));
    }
}
