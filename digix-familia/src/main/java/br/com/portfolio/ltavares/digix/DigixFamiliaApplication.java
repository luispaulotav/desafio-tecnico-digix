package br.com.portfolio.ltavares.digix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableFeignClients
@SpringBootApplication
public class DigixFamiliaApplication {

    public static void main(String[] args) { SpringApplication.run(DigixFamiliaApplication.class, args); }
}
