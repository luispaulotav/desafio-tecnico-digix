package br.com.portfolio.ltavares.digix.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FamiliaDTO {

    @Valid
    @NotEmpty(message = "pessoas não pode ser vazio")
    private List<PessoaDTO> pessoas;
}

