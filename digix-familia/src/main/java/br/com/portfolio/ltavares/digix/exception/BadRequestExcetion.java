package br.com.portfolio.ltavares.digix.exception;

public class BadRequestExcetion extends RuntimeException {

    public BadRequestExcetion(String message) {
        super( message);
    }
}
