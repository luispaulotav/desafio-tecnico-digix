package br.com.portfolio.ltavares.digix.service.imp;


import br.com.portfolio.ltavares.digix.client.ControladorClient;
import br.com.portfolio.ltavares.digix.dto.FamiliaDTO;
import br.com.portfolio.ltavares.digix.dto.PessoaDTO;
import br.com.portfolio.ltavares.digix.dto.RendaDTO;
import br.com.portfolio.ltavares.digix.enumaration.Status;
import br.com.portfolio.ltavares.digix.enumaration.TipoVinculo;
import br.com.portfolio.ltavares.digix.exception.BadRequestExcetion;
import br.com.portfolio.ltavares.digix.exception.NotFoundException;
import br.com.portfolio.ltavares.digix.model.Familia;
import br.com.portfolio.ltavares.digix.model.Pessoa;
import br.com.portfolio.ltavares.digix.model.Renda;
import br.com.portfolio.ltavares.digix.repository.FamiliaRepository;
import br.com.portfolio.ltavares.digix.service.interfaces.IFamiliaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FamiliaServiceImp implements IFamiliaService {

    private final FamiliaRepository familiaRepository;

    private final ControladorClient controladorClient;

    @Autowired
    public FamiliaServiceImp(FamiliaRepository familiaRepository, ControladorClient controladorClient) {
        this.familiaRepository = familiaRepository;
        this.controladorClient = controladorClient;
    }

    @Override
    public Familia cadastrar(FamiliaDTO familiaCadastro)  {
        try {
            Familia familia = familiaRepository.save(buildFamilia(familiaCadastro));
            enviarFamiliaParaControlador(familia);
            return familia;
        }catch (BadRequestExcetion e){
            throw new BadRequestExcetion("erro ao tentar cadastrar a familia. Por favor contate o suporte.");
        }
    }

    @Override
    public Familia editar(FamiliaDTO familiaDTO, UUID uuid) {
        try {
            Optional<Familia> optionalFamilia = findByIdAndStatus(uuid, Status.CADASTROIMCOMPLETO);
            Familia familia = updateValues(familiaDTO, optionalFamilia.get());
            this.familiaRepository.save(familia);
            enviarFamiliaParaControlador(familia);
            return familia;
        } catch (BadRequestExcetion e){
            throw new BadRequestExcetion("erro ao tentar editar a familia. Por favor contate o suporte.");
        }
    }

    @Override
    public boolean Excluir(UUID familaId) {
        try {
            Optional<Familia> optionalFamilia = findByIdAndStatus(familaId, Status.CADASTROIMCOMPLETO);
            this.familiaRepository.delete(optionalFamilia.get());
            return true;
        } catch (BadRequestExcetion e){
            throw new BadRequestExcetion("erro ao tentar excluir a familia. Por favor contate o suporte.");
        }
    }

    @Override
    public Optional<Familia> buscarFamiliaPorID(UUID familaId) {
        return Optional.ofNullable(this.familiaRepository.findById(familaId)
                .orElseThrow(() -> new NotFoundException("Não foi encontrado familia com id "+ familaId+".")));
    }

    @Override
    public Page<Familia> buscaFamilias(Pageable pageable) {
        return this.familiaRepository.findAll(pageable);
    }

    private void enviarFamiliaParaControlador(Familia familia) {
        if(familia != null && familia.getId() != null) {
            controladorClient.receiveMessage(familia.getId());
        }
    }

    public Optional<Familia> findByIdAndStatus(UUID uuid, Status status) {
        return Optional.ofNullable(this.familiaRepository.findByIdAndStatus(uuid, status))
                .orElseThrow(() -> new NotFoundException("Não foi encontrado familia com id "+ uuid
                        + " ou o cadastro não está com Status igual a Cadastro Imcompleto." ));
    }

    private Familia updateValues(FamiliaDTO atual, Familia original) {
        original.setPessoas(transformaPessoasDtoParaPessoas(atual.getPessoas()));
        return original;
    }

    private List<Pessoa> transformaPessoasDtoParaPessoas(List<PessoaDTO> pessoas) {
       return buildPessoas(pessoas);
    }

    private Familia buildFamilia(FamiliaDTO familiaCadastro) {
        Familia familia = Familia.builder()
                .pessoas(buildPessoas(familiaCadastro.getPessoas()))
                .build();
        if (!possuiLimiteConjugue(familiaCadastro)) {
            familia.setStatus(Status.CADASTROIMCOMPLETO);
        } else {
            familia.setStatus(Status.CADASTROVALIDO);
        }
        return familia;
    }

    public boolean possuiLimiteConjugue(FamiliaDTO familiaDTO){
        List<PessoaDTO> pessoasConjugueDTO =  familiaDTO.getPessoas()
                .stream()
                .filter(pessoaContempladoDTO ->
                        TipoVinculo.CONJUGUE.equals(pessoaContempladoDTO.getTipoVinculo()))
                .collect(Collectors.toList());

        List<PessoaDTO> pessoasPretendenteDTO =  familiaDTO.getPessoas()
                .stream()
                .filter(pessoaContempladoDTO ->
                        TipoVinculo.PRETENDENTE.equals(pessoaContempladoDTO.getTipoVinculo()))
                .collect(Collectors.toList());
        return pessoasConjugueDTO.size() == 1 && pessoasPretendenteDTO.size() == 1;
    }


    private List<Pessoa> buildPessoas(List<PessoaDTO> pessoaDTOList) {
        return pessoaDTOList.stream().map(this::buildPessoa).collect(Collectors.toList());
    }

    private Pessoa buildPessoa(PessoaDTO pessoaDTO) {
        return  Pessoa.builder()
                .nome(pessoaDTO.getNome())
                .dataNascimento(pessoaDTO.getDataNascimento())
                .renda(buildRenda(pessoaDTO.getRenda()))
                .tipoVinculo(pessoaDTO.getTipoVinculo())
                .build();
    }

    private Renda buildRenda(RendaDTO rendaDTO) {
        return Renda.builder()
                .valor(rendaDTO.getValor())
                .build();
    }
}