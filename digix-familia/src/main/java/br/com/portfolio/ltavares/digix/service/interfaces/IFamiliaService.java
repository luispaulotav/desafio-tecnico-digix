package br.com.portfolio.ltavares.digix.service.interfaces;

import br.com.portfolio.ltavares.digix.dto.FamiliaDTO;
import br.com.portfolio.ltavares.digix.model.Familia;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IFamiliaService {
    Familia cadastrar(FamiliaDTO familiaCadastro);

    Familia editar(FamiliaDTO familia, UUID uuid);

    boolean Excluir(UUID familaId);

    Optional<Familia> buscarFamiliaPorID(UUID familaId);

    Page<Familia> buscaFamilias(Pageable pageable);
}
