package br.com.portfolio.ltavares.digix.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.UUID;

@FeignClient(name = "controlador-service", url = "${controlador.service.url}", decode404 = true)
public interface ControladorClient {

    @PostMapping("/receive/{uuid}")
    ResponseEntity<?> receiveMessage(@PathVariable("uuid") UUID uuid);
}

