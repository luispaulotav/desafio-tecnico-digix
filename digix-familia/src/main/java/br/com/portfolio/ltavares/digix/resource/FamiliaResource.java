package br.com.portfolio.ltavares.digix.resource;

import br.com.portfolio.ltavares.digix.dto.FamiliaDTO;
import br.com.portfolio.ltavares.digix.model.Familia;
import br.com.portfolio.ltavares.digix.service.imp.FamiliaServiceImp;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/familias")
public class FamiliaResource {

    @Autowired
    private FamiliaServiceImp familiaServiceImp;

    @PostMapping(value = "/cadastrar", produces = "application/json")
    @ApiOperation(value = "Cadastrar familia.", response = Familia.class)
    public ResponseEntity<Familia> cadastrarFamilia(@Valid @RequestBody FamiliaDTO familiaDTO) {
        return ResponseEntity.ok(this.familiaServiceImp.cadastrar(familiaDTO));

    }

    @PutMapping(value = "/editar/{uuid}", produces = "application/json")
    @ApiOperation(value = "Editar familia.", response = Familia.class)
    public ResponseEntity<Familia> editarFamilia(@Valid @RequestBody FamiliaDTO familiaDTO,
                                                 @ApiParam(value="uuid") @PathVariable("uuid") UUID uuid) {
        return ResponseEntity.ok(this.familiaServiceImp.editar(familiaDTO, uuid));
    }

    @DeleteMapping(value = "/excluir/{uuid}", produces = "application/json")
    @ApiOperation(value = "Excluir Familia.", response = boolean.class)
    public ResponseEntity<?> excluirFamilia(@ApiParam(value="uuid") @PathVariable("uuid") UUID FamilaId) {
        return ResponseEntity.ok(this.familiaServiceImp.Excluir(FamilaId));
    }

    @GetMapping(value = "/buscar/{uuid}", produces = "application/json")
    @ApiOperation(value = "Buscar familia cadastrada por ID.", response = Familia.class)
    public ResponseEntity<?> buscarFamiliaPorID(@ApiParam(value="uuid") @PathVariable("uuid") UUID FamilaId) {
        return ResponseEntity.ok(this.familiaServiceImp.buscarFamiliaPorID(FamilaId));
    }

    @GetMapping(value = "/buscatodas", produces = "application/json")
    @ApiOperation(value = "Buscar todas familias", response = Familia.class)
    public ResponseEntity<?> buscaFamilias(Pageable pageable) {
        return ResponseEntity.ok(this.familiaServiceImp.buscaFamilias(pageable));
    }

}
