package br.com.portfolio.ltavares.digix.dto;

import br.com.portfolio.ltavares.digix.enumaration.TipoVinculo;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PessoaDTO implements Serializable {

    @NotEmpty(message = "nome não pode ser vazio.")
    @NotNull(message = "nome não deve ser nulo.")
    private String nome;

    @ApiModelProperty(required = true, example = "27/11/1990")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dataNascimento;

    @NotNull(message = "tipoVinculo não deve ser nulo.")
    private TipoVinculo tipoVinculo;

    @Valid
    @NotNull(message = "renda não deve ser nulo.")
    private RendaDTO renda;
}
