package br.com.portfolio.ltavares.digix.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RendaDTO implements Serializable {

    @NotNull(message = "valor da renda não deve ser nulo.")
    @Min(value = 0, message = "O campo valor da renda deve ser no minimo 0.")
    private Double valor;
}
