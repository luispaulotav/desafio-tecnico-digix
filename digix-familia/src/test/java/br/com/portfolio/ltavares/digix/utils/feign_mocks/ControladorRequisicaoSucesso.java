package br.com.portfolio.ltavares.digix.utils.feign_mocks;

import br.com.portfolio.ltavares.digix.client.ControladorClient;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public class ControladorRequisicaoSucesso implements ControladorClient {

    @Override
    public ResponseEntity<?> receiveMessage(UUID uuid) {
        return ResponseEntity.ok().build();
    }
}
