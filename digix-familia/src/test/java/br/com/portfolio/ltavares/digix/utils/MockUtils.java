package br.com.portfolio.ltavares.digix.utils;

import br.com.portfolio.ltavares.digix.dto.FamiliaDTO;
import br.com.portfolio.ltavares.digix.dto.PessoaDTO;
import br.com.portfolio.ltavares.digix.dto.RendaDTO;
import br.com.portfolio.ltavares.digix.enumaration.Status;
import br.com.portfolio.ltavares.digix.enumaration.TipoVinculo;
import br.com.portfolio.ltavares.digix.model.Familia;
import br.com.portfolio.ltavares.digix.model.Pessoa;
import br.com.portfolio.ltavares.digix.model.Renda;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MockUtils {

    public static Familia buildFamilia(){
        return Familia.builder()
                .pessoas(Arrays.asList(buildPessoa()))
                .status(Status.CADASTROIMCOMPLETO)
                .build();
    }

    private static Pessoa buildPessoa() {
        return Pessoa.builder()
                .dataNascimento(LocalDate.of(1990,11,24))
                .tipoVinculo(TipoVinculo.PRETENDENTE)
                .nome("Jose Antonio da Silva")
                .renda(buildRenda())
                .build();
    }

    private static Renda buildRenda() {
        return Renda.builder()
                .valor(1900.0)
                .build();
    }

    public static FamiliaDTO buildFamiliaDTO(){
        return FamiliaDTO.builder()
                .pessoas(Arrays.asList(buildPessoaDTO()))
                .build();
    }

    private static PessoaDTO buildPessoaDTO() {
        return PessoaDTO.builder()
                .nome("Jose Antonio da Silva")
                .dataNascimento(LocalDate.of(1990, 11, 24))
                .tipoVinculo(TipoVinculo.PRETENDENTE)
                .renda(buildRendaDTO())
                .build();
    }

    private static RendaDTO buildRendaDTO() {
        return RendaDTO.builder()
                .valor(1900.0)
                .build();
    }

    public static Familia updateValues(FamiliaDTO atual, Familia original) {
        original.setPessoas(transformaPessoasDtoParaPessoas(atual.getPessoas()));
        return original;
    }

    public static List<Pessoa> transformaPessoasDtoParaPessoas(List<PessoaDTO> pessoasdto) {
        List<Pessoa> pessoas = new ArrayList<>();
        pessoasdto.forEach(pessoaDTO -> {
            Pessoa pessoa = buildPessoa(pessoaDTO);
            pessoas.add(pessoa);
        });
        return pessoas;
    }

    private static Pessoa buildPessoa(PessoaDTO pessoaDTO) {
        return  Pessoa.builder()
                .nome(pessoaDTO.getNome())
                .dataNascimento(pessoaDTO.getDataNascimento())
                .renda(transformaRendaDtoParaRenda(pessoaDTO.getRenda()))
                .tipoVinculo(pessoaDTO.getTipoVinculo())
                .build();
    }

    private static Renda transformaRendaDtoParaRenda(RendaDTO rendaDTO) {
        return Renda.builder()
                .valor(rendaDTO.getValor())
                .build();
    }
}
