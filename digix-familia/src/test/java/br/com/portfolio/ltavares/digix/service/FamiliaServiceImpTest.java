package br.com.portfolio.ltavares.digix.service;

import br.com.portfolio.ltavares.digix.client.ControladorClient;
import br.com.portfolio.ltavares.digix.dto.FamiliaDTO;
import br.com.portfolio.ltavares.digix.enumaration.Status;
import br.com.portfolio.ltavares.digix.model.Familia;
import br.com.portfolio.ltavares.digix.repository.FamiliaRepository;
import br.com.portfolio.ltavares.digix.service.imp.FamiliaServiceImp;
import br.com.portfolio.ltavares.digix.utils.MockUtils;
import br.com.portfolio.ltavares.digix.utils.feign_mocks.ControladorRequisicaoSucesso;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FamiliaServiceImpTest {

    @InjectMocks
    private FamiliaServiceImp familiaServiceImp;

    @Mock
    private FamiliaRepository familiaRepository;

    @Spy
    private ControladorClient controladorClient = new ControladorRequisicaoSucesso();

    private FamiliaDTO familiaDTO;

    private Familia familia;

    List<Familia> familias = new ArrayList<>();

    @Before
    public void init(){
       familiaDTO = MockUtils.buildFamiliaDTO();
       familia = MockUtils.buildFamilia();
       familia.setId(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));
       familias.add(familia);
    }

    @Test
    public void cadastrarFamiliaTest(){
        when(familiaRepository.save(familia)).thenReturn(familia);
        familiaServiceImp.cadastrar(familiaDTO);

        assertEquals(familiaDTO.getPessoas().get(0).getNome(), familia.getPessoas().get(0).getNome());
        assertEquals(familiaDTO.getPessoas().get(0).getDataNascimento(), familia.getPessoas().get(0).getDataNascimento());
        assertEquals(familiaDTO.getPessoas().get(0).getTipoVinculo(), familia.getPessoas().get(0).getTipoVinculo());
        assertEquals(familiaDTO.getPessoas().get(0).getRenda().getValor(), familia.getPessoas().get(0).getRenda().getValor());
    }

    @Test
    public void editarFamiliaTest(){
        when(familiaRepository.findByIdAndStatus(familia.getId(), Status.CADASTROIMCOMPLETO)).thenReturn(Optional.ofNullable(familia));
        when(familiaRepository.save(familia)).thenReturn(familia);

        familiaDTO.getPessoas().get(0).getRenda().setValor(2500.0);
        MockUtils.updateValues(familiaDTO, familia);

        familiaServiceImp.editar(familiaDTO, UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));

        assertEquals(familiaDTO.getPessoas().get(0).getNome(), familia.getPessoas().get(0).getNome());
        assertEquals(familiaDTO.getPessoas().get(0).getDataNascimento(), familia.getPessoas().get(0).getDataNascimento());
        assertEquals(familiaDTO.getPessoas().get(0).getTipoVinculo(), familia.getPessoas().get(0).getTipoVinculo());
        assertEquals(familiaDTO.getPessoas().get(0).getRenda().getValor(), familia.getPessoas().get(0).getRenda().getValor());
    }

    @Test
    public void excluirFamiliaTest(){
        when(this.familiaRepository.findByIdAndStatus(familia.getId(), Status.CADASTROIMCOMPLETO)).thenReturn(Optional.ofNullable(familia));
        doNothing().when(familiaRepository).delete(any(Familia.class));
        boolean familiaExcluido = familiaServiceImp.Excluir(familia.getId());

        assertEquals(familiaExcluido, true);
    }

    @Test
    public void buscarFamiliaPorIdTeste(){
       when(this.familiaRepository.findById(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"))).thenReturn(Optional.ofNullable(familia));
       this.familiaServiceImp.buscarFamiliaPorID(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));

        assertEquals(familiaDTO.getPessoas().get(0).getNome(), familia.getPessoas().get(0).getNome());
        assertEquals(familiaDTO.getPessoas().get(0).getDataNascimento(), familia.getPessoas().get(0).getDataNascimento());
        assertEquals(familiaDTO.getPessoas().get(0).getTipoVinculo(), familia.getPessoas().get(0).getTipoVinculo());
        assertEquals(familiaDTO.getPessoas().get(0).getRenda().getValor(), familia.getPessoas().get(0).getRenda().getValor());
    }

    @Test
    public void buscaTodosUsuariosTeste(){
        Page<Familia> famiiasPage = new PageImpl<>(familias);
        when(familiaRepository.findAll(famiiasPage.getPageable()))
                .thenReturn(famiiasPage);
        familiaServiceImp.buscaFamilias(famiiasPage.getPageable());

        assertNotNull(famiiasPage);
        assertEquals(famiiasPage.getTotalElements(), familias.size());
        assertEquals(famiiasPage.getContent().get(0).getId(), familias.get(0).getId());
    }

}
