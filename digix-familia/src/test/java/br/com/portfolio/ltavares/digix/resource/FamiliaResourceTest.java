package br.com.portfolio.ltavares.digix.resource;

import br.com.portfolio.ltavares.digix.dto.FamiliaDTO;
import br.com.portfolio.ltavares.digix.model.Familia;
import br.com.portfolio.ltavares.digix.service.imp.FamiliaServiceImp;
import br.com.portfolio.ltavares.digix.utils.MockUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FamiliaResourceTest {

    @InjectMocks
    private FamiliaResource resource;

    @Mock
    private FamiliaServiceImp familiaServiceImp;

    private FamiliaDTO familiaDTO;

    private Familia familia;

    private List<Familia> familias = new ArrayList<>();

    @Before
    public void init() {
        familiaDTO = MockUtils.buildFamiliaDTO();
        familia = MockUtils.buildFamilia();
        familia.getPessoas().get(0).setId(UUID.fromString("feae3495-91f7-46e9-8993-fabbb3b9942b"));
        familia.getPessoas().get(0).getRenda().setId(UUID.fromString("cae23453-0927-4de5-bd20-6dadc0512fb9"));
        familia.setId(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));
        familias.add(familia);
    }

    @Test
    public void cadastrarFamiliaTeste() {
        when(this.familiaServiceImp.cadastrar(familiaDTO)).thenReturn(familia);
        ResponseEntity response = resource.cadastrarFamilia(familiaDTO);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void editarFamiliaTeste() {
        when(this.familiaServiceImp.editar(familiaDTO, UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"))).thenReturn(familia);
        ResponseEntity response = resource.editarFamilia(familiaDTO, UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void excluirUsuarioTeste() {
        when(this.familiaServiceImp.Excluir(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"))).thenReturn(true);
        ResponseEntity response = resource.excluirFamilia(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void buscaFamiliaPorIdTeste() {
        when(this.familiaServiceImp.buscarFamiliaPorID(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"))).thenReturn(Optional.of(familia));
        ResponseEntity response = resource.buscarFamiliaPorID(UUID.fromString("4f0b3fe6-24fb-4bff-baf3-2026a21f1f3b"));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void buscaTodasFamiliasTeste() {
       // when(this.familiaServiceImp.buscaFamilias(isA(Pageable.class))).thenReturn(new PageImpl<>(familias));
        ResponseEntity response = resource.buscaFamilias(isA(Pageable.class));
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

}
