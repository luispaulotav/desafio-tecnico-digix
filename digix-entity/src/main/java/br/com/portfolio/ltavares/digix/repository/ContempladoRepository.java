package br.com.portfolio.ltavares.digix.repository;

import br.com.portfolio.ltavares.digix.model.Contemplado;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ContempladoRepository extends JpaRepository<Contemplado, UUID> {
}
