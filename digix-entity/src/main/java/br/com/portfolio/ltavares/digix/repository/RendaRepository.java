package br.com.portfolio.ltavares.digix.repository;

import br.com.portfolio.ltavares.digix.model.Renda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RendaRepository extends JpaRepository<Renda, UUID> {
}