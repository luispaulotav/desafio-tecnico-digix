package br.com.portfolio.ltavares.digix.model;

import br.com.portfolio.ltavares.digix.enumaration.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Familia extends BaseEntity {

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "familia_id")
    private List<Pessoa> pessoas;

    @Enumerated(EnumType.ORDINAL)
    private Status status;
}

