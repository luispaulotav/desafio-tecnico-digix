package br.com.portfolio.ltavares.digix.enumaration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Status {

    CADASTROVALIDO(0, "Cadastro Válido"),
    POSSUICASA(1, "Já Possui uma casa"),
    SELECIONADOEMOUTROPROCESSO(2, "Selecionada em outro processo de seleção"),
    CADASTROIMCOMPLETO(3,"Cadastro Incompleto");

    private final Integer id;
    private final String valor;
}
