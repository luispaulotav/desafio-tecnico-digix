package br.com.portfolio.ltavares.digix.repository;

import br.com.portfolio.ltavares.digix.enumaration.Status;
import br.com.portfolio.ltavares.digix.model.Familia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface FamiliaRepository extends JpaRepository<Familia, UUID> {

    Optional<Familia> findByIdAndStatus(UUID uuid, Status status);

}
