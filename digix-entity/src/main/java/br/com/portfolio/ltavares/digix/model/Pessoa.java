package br.com.portfolio.ltavares.digix.model;

import br.com.portfolio.ltavares.digix.enumaration.TipoVinculo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Pessoa extends BaseEntity {

    private String nome;

    @Column(name = "data_nascimento", nullable = false)
    private LocalDate dataNascimento;

    @Enumerated(EnumType.ORDINAL)
    private TipoVinculo tipoVinculo;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Renda renda;
}
