package br.com.portfolio.ltavares.digix.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Contemplado extends BaseEntity {

    private UUID identificadorFamilia;

    private int quantidadeCriterio;

    private int pontuacaoTotal;

    private LocalDate dataFamilaSelecionada;

}
