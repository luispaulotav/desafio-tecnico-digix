package br.com.portfolio.ltavares.digix.enumaration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoVinculo {

    PRETENDENTE(0,"Pretendente"),
    CONJUGUE(1,"Cônjugue"),
    DEPENDENTE(2,"Dependente");

    private final Integer id;
    private final String valor;
}

