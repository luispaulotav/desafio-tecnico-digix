package br.com.portfolio.ltavares.digix.enumaration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ValorRenda {

    RENDAATE900(0, 5),
    RENDA901A1500(1, 3),
    RENDA1501A2000(2, 1);

    private final Integer id;
    private final int pontuação;

    public static int getPontuacaoPorValorRenda(double valor){
        if(valor < 900) {
            return RENDAATE900.getPontuação();
        }
        if(valor > 900 && valor <= 1500) {
            return RENDA901A1500.getPontuação();
        }
        if(valor > 1500 && valor <= 2000){
            return RENDA1501A2000.getPontuação();
        }
        return 0;
    }
}
