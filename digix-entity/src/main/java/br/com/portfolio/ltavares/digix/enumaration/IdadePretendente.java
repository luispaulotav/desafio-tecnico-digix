package br.com.portfolio.ltavares.digix.enumaration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IdadePretendente {

    IDADEABAIXO30(0, 1),
    IDADE30a44(1, 2),
    IDADEIGUALOUACIMA45(2, 3);

    private final Integer id;
    private final int pontuação;

    public static int getPontuacaoPorIdade(int idade){
        if(idade <= 30) {
            return IDADEABAIXO30.getPontuação();
        }
        if(idade > 30 && idade < 44) {
            return IDADE30a44.getPontuação();
        }
        if(idade >= 45) {
            return IDADEIGUALOUACIMA45.getPontuação();
        }
        return 0;
    }
}
