# Desafio Técnico Digix

A aplicação **Desafio Técnico Digix** é uma aplicação para gerenciamento de contemplação de casa popular para respectivas familias do estado.

# Tecnologias

Para criar o sistema foram utilizados as seguintes ferramentas/frameworks:

- Backend com Spring-boot v2.3+ e Java v8

# Composição da Stack

A Stack do Desafio Técnico Digix é composta por uma aplicação (apenas backend) composta por módulos:
- digix-entity: Responsável em centralizar as entidades em um unico pacote, facilitando a utilização em todo o projeto sem necessidade de duplicidade. 

- digix-familia: Responsável em cadastrar a familia, fazendo apenas a função de formulario (CRUD).  
- digix-controlador: Responsável em controlar as requisições do módulo familia, através do software (RabbitMQ), onde o mesmo controla as requisições em uma fila sem deixar a requisição aguardando.
- digix-contemplado: Responsavél em controlar as familias contempladas.


## O que preciso para subir a aplicação

- Gerenciador de dependencias e build, Maven.

- Java 8.

- Banco de dados postgresql

## Gerando o Pacote
Sendo um projeto Maven, execute os goals clean e install na raiz do projeto para baixar as dependências e gerar jar do projeto

     #!/desafio-tecnico-digix
     $ mvn clean install
     
## Executando o Jar

Como se trata de um projeto Spring Boot, podemos simplismente executar o jar que foi gerado na pasta target e a 
aplicação irá subir em um tomcat embedded.

    #!/desafio-tecnico-digix/target
    $ java -jar desafio-digito-unico-0.0.1-SNAPSHOT.jar
    
## Executando os Testes Unitários

    #!/desafio-tecnico-digix
    $ mvn test    

Configuração da porta da api digix-familia se encontra no application.yml:
		
	server:
	    port: 8080

Configuração da porta da api digix-controlador se encontra no application.yml:
		
	server:
	    port: 8081
	    
Configuração da porta da api digix-contemplado se encontra no application.yml:
		
	server:
	    port: 8082
	    

## Consumir API

Primeiramente devemos caddastrar a Familia acessando a url http://localhost:8080/familias/cadastrar.

### Exemplos:

#### Create familias/cadastrar:

Entrada:

http://localhost:8080/familias/cadastrar

    {
      "pessoas": [
        {
          "dataNascimento": "27/01/1970",
          "nome": "Jose",
          "renda": {
            "valor": 750
          },
          "tipoVinculo": "PRETENDENTE"
        },
         {
          "dataNascimento": "27/01/1987",
          "nome": "Maria",
          "renda": {
            "valor": 2000
          },
          "tipoVinculo": "CONJUGUE"
        }
      ]
    }
    
Saída: 200 OK

    {
        "id": "396f21cf-97c1-4170-9368-624c6901e500",
        "pessoas": [
            {
                "id": "49095989-5322-4b91-bdf5-560d44f30afe",
                "nome": "Jose",
                "dataNascimento": "1970-01-27",
                "tipoVinculo": "PRETENDENTE",
                "renda": {
                    "id": "55a8ee2d-0401-444e-b771-17eae586f24a",
                    "valor": 750
                }
            },
            {
                "id": "54cc6317-2fcd-4b59-8f5b-86bc8d737cc7",
                "nome": "Maria",
                "dataNascimento": "1987-01-27",
                "tipoVinculo": "CONJUGUE",
                "renda": {
                    "id": "76e606ac-2727-4b4d-987f-34f06a9f544b",
                    "valor": 2000
                }
            }
        ],
        "status": "CADASTROVALIDO"
    }
    

#### contemplados/buscartodos:

Entrada:

http://localhost:8082/contemplados/buscartodos:
        
Saída: OK

       {
           "content": [
               {
                   "id": "c1fcd6e3-e30e-4935-8a59-7a2fda0d4c4a",
                   "identificadorFamilia": "29556a54-8815-4abc-aef8-4f8b7e8df90a",
                   "quantidadeCriterio": 4,
                   "pontuacaoTotal": 13,
                   "dataFamilaSelecionada": "2020-08-18"
               },
               {
                   "id": "1b28d9d7-a187-44b5-a42f-06c350e956c7",
                   "identificadorFamilia": "396f21cf-97c1-4170-9368-624c6901e500",
                   "quantidadeCriterio": 4,
                   "pontuacaoTotal": 13,
                   "dataFamilaSelecionada": "2020-08-18"
               }
           ],
           "pageable": {
               "sort": {
                   "sorted": false,
                   "unsorted": true,
                   "empty": true
               },
               "offset": 0,
               "pageNumber": 0,
               "pageSize": 20,
               "paged": true,
               "unpaged": false
           },
           "totalElements": 2,
           "last": true,
           "totalPages": 1,
           "size": 20,
           "number": 0,
           "sort": {
               "sorted": false,
               "unsorted": true,
               "empty": true
           },
           "numberOfElements": 2,
           "first": true,
           "empty": false
       }